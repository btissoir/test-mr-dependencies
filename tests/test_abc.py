import os.path

def test_exists():
    assert os.path.isfile('src/abc.txt')

def test_exists_1():
    assert os.path.isfile('src/abc_1.txt')

def test_exists_2():
    assert os.path.isfile('src/abc_2.txt')
